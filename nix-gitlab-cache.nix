{
  writeShellApplication,
  coreutils,
  findutils,
}:
writeShellApplication {
  name = "nix-gitlab-cache";
  runtimeInputs = [coreutils findutils];
  text = ''
    main() {
      if [[ $# -lt 1 ]]; then
        cmd_help
        exit 1
      fi

      case "$1" in

      archive-added)
        cmd_archive_added
        ;;

      archive-marked)
        cmd_archive_marked
        ;;

      mark)
        shift
        cmd_mark "$@"
        ;;

      list-marked)
        cmd_list_marked
        ;;

      list-added)
        cmd_list_added
        ;;

      help)
        cmd_help
        ;;

      *)
        echo "Invalid command '$1' provided" 2>&1
        cmd_help
        exit 1
        ;;

      esac
    }

    cmd_help() {
      log "Usage: $0 <cmd> [ args ]"
      log
      log "  Where <cmd>:"
      log "  - mark [ path [ path ... ] ]: mark the paths for caching"
      log "  - archive-marked: copy the marked derivations to the cache"
      log "  - archive-added: copy everything that has been added to the store since the start"
      log "  - list-marked: list paths marked for archival"
      log "  - list-added: list paths that were added to the store since the start"
    }

    cmd_mark() {
      local sink
      sink="$(create_paths_file)"

      for path in "$@"; do
        echo "$path" >> "$sink"
      done
    }

    cmd_archive_marked() {
      cmd_list_marked | xargs -r nix store sign -k "$CI_NIX_CACHE_PRIVATE_KEY_PATH"
      cmd_list_marked | xargs -r nix copy --to "$CI_NIX_CACHE"
    }

    cmd_archive_added() {
      cmd_list_added | xargs -r nix store sign -k "$CI_NIX_CACHE_PRIVATE_KEY_PATH"
      cmd_list_added | xargs -r nix copy --to "$CI_NIX_CACHE"
    }

    cmd_list_marked() {
      # avoid cat not finding any files
      create_paths_file >/dev/null
      cat "/tmp/nix-marked-paths."* | sort | uniq
    }

    cmd_list_added() {
      comm -13 /usr/share/nix/orig-store-content <(list-nix-store)
    }

    log() {
      echo "$@" 2>&1
    }

    create_paths_file() {
      mktemp /tmp/nix-marked-paths.XXXXXX
    }

    main "$@"
  '';
}
