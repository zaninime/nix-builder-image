let
  pkgs = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/refs/heads/nixpkgs-unstable.tar.gz";
  }) {};

  cacheScript = pkgs.callPackage ./nix-gitlab-cache.nix {};

  privilegedSetup = pkgs.writeShellApplication {
    name = "privileged-setup";
    text = ''
      if [[ "''${CI_DEBUG_TRACE:-}" == "true" ]]; then
        set -x
      fi

      chown root "$USER_VARS_FILE"
      chmod 0666 "$USER_VARS_FILE"

      add_user_var() {
        echo "export $1=''${2@Q}" >> "$USER_VARS_FILE"
      }

      if [[ -n "''${CI_NIX_CACHE_PRIVATE_KEY:-}" ]]; then
        CI_NIX_CACHE_PRIVATE_KEY_PATH="$(mktemp -t nix-gitlab-cache-key.XXXXXX)"
        add_user_var CI_NIX_CACHE_PRIVATE_KEY_PATH "$CI_NIX_CACHE_PRIVATE_KEY_PATH"
        echo "$CI_NIX_CACHE_PRIVATE_KEY" > "$CI_NIX_CACHE_PRIVATE_KEY_PATH"
        chown "$TARGET_USER" "$CI_NIX_CACHE_PRIVATE_KEY_PATH"
        echo "secret-key-files = $CI_NIX_CACHE_PRIVATE_KEY_PATH" >> /etc/nix/nix.conf
      fi

      if [[ -n "''${CI_NIX_LOCAL_CACHE_PATH:-}" ]]; then
        full_path="$CI_PROJECT_DIR/$CI_NIX_LOCAL_CACHE_PATH"
        compression="''${CI_NIX_LOCAL_CACHE_COMPRESSION:-none}"
        mkdir -p "$full_path"
        CI_NIX_CACHE="file://$full_path?compression=$compression"
        add_user_var CI_NIX_CACHE "$CI_NIX_CACHE"
        echo "extra-substituters = $CI_NIX_CACHE" >> /etc/nix/nix.conf
      elif [[ -n "''${CI_NIX_CACHE:-}" ]]; then
        echo "extra-substituters = $CI_NIX_CACHE" >> /etc/nix/nix.conf
      fi
    '';
  };
in
  pkgs.writeShellApplication {
    name = "entrypoint";
    runtimeInputs = with pkgs; [bash coreutils gitMinimal cacheScript];
    text = ''
      if [[ "''${CI_DEBUG_TRACE:-}" == "true" ]]; then
        set -x
      fi

      USER_VARS_FILE="$(mktemp)"
      export USER_VARS_FILE
      export TARGET_USER="$USER"
      sudo -E ${privilegedSetup}/bin/${privilegedSetup.name}
      # shellcheck source=/dev/null
      source "$USER_VARS_FILE"
      sudo rm -f "$USER_VARS_FILE"
      unset USER_VARS_FILE

      # shellcheck source=/dev/null
      . "/home/$USER/.nix-profile/etc/profile.d/nix.sh"

      exec "$@"
    '';
  }
