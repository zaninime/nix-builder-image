FROM alpine:3

ENV USER=builder

COPY prepare-env.sh install-nix.sh entrypoint.nix nix-gitlab-cache.nix /opt/nix/
COPY list-store.sh /bin/list-nix-store

RUN cd /opt/nix \
 && chmod a+rx /bin/list-nix-store \
 && chmod a+r * \
 && chmod a+rx *.sh \
 && ./prepare-env.sh

USER "$USER"
WORKDIR "/home/$USER"

RUN /opt/nix/install-nix.sh \
 && sudo rm -rf /opt/nix

ENTRYPOINT [ "/bin/entrypoint" ]
CMD [ "/bin/sh" ]
