#/usr/bin/env sh

set -euo pipefail

apk add --update-cache curl sudo xz
adduser -u 1000 -D "$USER"
echo "$USER ALL=(ALL) NOPASSWD:ALL" | (EDITOR='tee -a' visudo)

rm -rf /var/cache/apk/*
