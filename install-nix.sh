#!/usr/bin/env sh

set -euo pipefail

workdir="$(mktemp -d)"
trap 'rm -rf "$workdir"' EXIT

my_dir="$(dirname "$(readlink -f "$0")")"

cat <<EOF > "$workdir/nix.conf"
max-jobs = auto
trusted-users = root $USER
experimental-features = nix-command flakes
build-users-group =
EOF

sudo mkdir -p /etc/nix
sudo chmod 0755 /etc/nix
sudo cp $workdir/nix.conf /etc/nix/nix.conf

curl -sSL --fail -o "$workdir/install" https://nixos.org/nix/install
sh "$workdir/install" --no-channel-add --nix-extra-conf-file "$workdir/nix.conf"

# shellcheck source=/dev/null
. /home/$USER/.nix-profile/etc/profile.d/nix.sh

entrypoint_pkg="$(nix-build --no-out-link "$my_dir/entrypoint.nix")"
sudo ln -s "$entrypoint_pkg/bin/entrypoint" /bin/entrypoint
ln -s "$entrypoint_pkg" /nix/var/nix/gcroots/nix-builder-image
nix-collect-garbage -d

echo ". /home/$USER/.nix-profile/etc/profile.d/nix.sh" >> "/home/$USER/.bashrc"

sudo mkdir -p /usr/share/nix

list-nix-store | sudo tee /usr/share/nix/orig-store-content >/dev/null
