#!/usr/bin/env sh

set -euo pipefail

find /nix/store -mindepth 1 -maxdepth 1 \
	! \( -iname '*.drv' -o -iname '*.drv.chroot' -o -iname '*.check' -o -iname '*.lock' -o -name '.links' \) \
	| sort
