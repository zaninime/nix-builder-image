#!/usr/bin/env bash

set -eu

echo "Should run as non-root"
[[ "$UID" -ne 0 ]]

echo "Should have coreutils available"
[[ "$(type -p cat)" =~ "coreutils" ]]

echo "Should run newer nix commands"
nix run 'nixpkgs#eza' -- -lhT "$PWD" >/dev/null

keyfile="$(mktemp)"
nix key generate-secret --key-name testing > "$keyfile"
export CI_NIX_CACHE_PRIVATE_KEY_PATH="$keyfile"

echo "Should archive marked paths"
nix build 'nixpkgs#hello'
nix-gitlab-cache mark "$(nix-store -q result)"
export CI_NIX_LOCAL_CACHE_PATH=".test-1"
export CI_NIX_CACHE="file://$CI_PROJECT_DIR/$CI_NIX_LOCAL_CACHE_PATH?compression=none"
nix-gitlab-cache archive-marked
[[ -e "$CI_PROJECT_DIR/$CI_NIX_LOCAL_CACHE_PATH/nix-cache-info" ]]

echo "Should archive all added derivations"
export CI_NIX_LOCAL_CACHE_PATH=".test-2"
export CI_NIX_CACHE="file://$CI_PROJECT_DIR/$CI_NIX_LOCAL_CACHE_PATH?compression=none"
sudo nix-collect-garbage -d
nix build --no-link 'nixpkgs#hello'
nix-gitlab-cache archive-added
sudo nix-collect-garbage -d
nix --extra-substituters "$CI_NIX_CACHE" build -L --no-link 'nixpkgs#hello' 2>&1 | tee build.log
if grep -q "cache.nixos.org" build.log; then
	echo "Caching did't work properly, as some paths were fetched from upstream"
	exit 1
fi
